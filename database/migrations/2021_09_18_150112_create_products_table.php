<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductsTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('products', function (Blueprint $table) {
			$table->id();
			$table->bigInteger('integration_id')->unique();
			$table->string('name');
			$table->float('price');
			$table->integer('amount')->default(0);
			$table->string('wh_code')->nullable();
			$table->string('wh_name')->nullable();
			$table->longText('images')->nullable();
//			$table->foreignId('catalog_id')->constrained('catalogs', 'integration_id');
			$table->string('brand')->nullable();
			$table->string('flavor')->nullable();
			$table->string('nomenclature')->nullable();
			$table->string('class')->nullable();
			$table->string('power')->nullable();
			$table->string('volume')->nullable();
			$table->string('with_flavors')->nullable();
			$table->string('made_in')->nullable();
			$table->string('item_no_discount')->nullable();
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('products');
	}
}
