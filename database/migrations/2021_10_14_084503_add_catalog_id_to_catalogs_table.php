<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddCatalogIdToCatalogsTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('catalogs', function (Blueprint $table) {
			$table
				->foreignId('catalog_id')
				->nullable()
				->constrained('catalogs', 'integration_id');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('catalogs', function (Blueprint $table) {
			$table->dropForeign('catalog_id');
		});
	}
}
