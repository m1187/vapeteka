<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('users', function (Blueprint $table) {
			$table->id();
			$table->string('name')->nullable();
			$table
				->string('phone_number')
				->nullable();
			$table
				->string('email')
				->nullable()
				->unique();
            $table->longText('qr_code')->nullable();
            $table->longText('qr_name')->nullable();
			$table->integer('code')->nullable();
			$table->string('password')->nullable();
			$table
				->string('integration_id')
				->nullable()
				->unique();
			$table->integer('denomination')->nullable();
			$table->boolean('cumulative')->default(true);
			$table->string('discount')->default(0);
			$table->float('preset')->default(0);
			$table->timestamp('phone_verified_at')->nullable();
			$table->timestamp('phone_verification_send')->nullable();
			$table->rememberToken();
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('users');
	}
}
