<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTelegramNotificationsTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('telegram_notifications', function (Blueprint $table) {
			$table->string('id');
			$table->string('notice');
			$table->string('noticedes');
			$table->string('noteicelink');
			$table->string('total_price');
			$table->string('telegramid');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('telegram_notifications');
	}
}
