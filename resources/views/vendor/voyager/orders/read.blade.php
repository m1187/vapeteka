@extends('voyager::master')

@section('page_title', __('voyager::generic.view').' '.$dataType->getTranslatedAttribute('display_name_singular'))
@section('page_header')
    <div class="container">
        <h2>Заказ #{{ $order->id }}</h2>
        <div class="row">
            <div class="col-md-12">
                <table class="table" style="margin-top:2rem">
                    <tr>
                        <th>ФИО</th>
                        <th>Телефон Номер</th>
                        <th>Итоговая сумма</th>
                        <th>Сумма со скидкой</th>
{{--                        <th>QR Code</th>--}}
                    </tr>
                    <tr>
                        <td>{{ $order->name }}</td>
                        <td>{{ $order->phone_number }}</td>
                        <td>{{ $order->total_price }}</td>
                        <td>{{ $order->bonus_price }}</td>
                        {{--                <td>   {!! QrCode::size(125)->generate((String)$user->qr_code); !!}</td>--}}
                    </tr>
                </table>
            </div>
        </div>
    </div>
    @include('voyager::multilingual.language-selector')
@stop




@section('content')
    <div class="container">
        <div class="page-content read container-fluid">
            <div class="row">
                <div class="col-md-12">
                    @foreach ($itemPrices as $item)
                        <div class="panel panel-bordered" style="padding-bottom:5px;">
                            <div class="panel-heading" style="border-bottom:0; margin-left:10px; padding:20px">
                                <h4>Товар: {{ $item->name }}</h4>
                                <ul>
                                    <li>Цена: {{ $item->price }}</li>
                                    <li>Количество: {{ $item->quantity }}</li>
                                </ul>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>

    {{-- Single delete modal --}}
    <div class="modal modal-danger fade" tabindex="-1" id="delete_modal" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"
                            aria-label="{{ __('voyager::generic.close') }}"><span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title"><i
                            class="voyager-trash"></i> {{ __('voyager::generic.delete_question') }} {{ strtolower($dataType->getTranslatedAttribute('display_name_singular')) }}
                        ?</h4>
                </div>
                <div class="modal-footer">
                    <form action="{{ route('voyager.'.$dataType->slug.'.index') }}" id="delete_form" method="POST">
                        {{ method_field('DELETE') }}
                        {{ csrf_field() }}
                        <input type="submit" class="btn btn-danger pull-right delete-confirm"
                               value="{{ __('voyager::generic.delete_confirm') }} {{ strtolower($dataType->getTranslatedAttribute('display_name_singular')) }}">
                    </form>
                    <button type="button" class="btn btn-default pull-right"
                            data-dismiss="modal">{{ __('voyager::generic.cancel') }}</button>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
@stop

@section('javascript')
    @if ($isModelTranslatable)
        <script>
            $(document).ready(function () {
                $('.side-body').multilingual();
            });
        </script>
    @endif
    <script>
        var deleteFormAction;
        $('.delete').on('click', function (e) {
            var form = $('#delete_form')[0];

            if (!deleteFormAction) {
                // Save form action initial value
                deleteFormAction = form.action;
            }

            form.action = deleteFormAction.match(/\/[0-9]+$/)
                ? deleteFormAction.replace(/([0-9]+$)/, $(this).data('id'))
                : deleteFormAction + '/' + $(this).data('id');

            $('#delete_modal').modal('show');
        });

    </script>
@stop
