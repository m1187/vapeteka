<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\OrderController;
use App\Http\Controllers\NotificationController;

Route::post('/sendNotification/{id}', [NotificationController::class, 'store'])->name('notification.store');
Route::post('/sendNotificationForOneUser/{id}', [NotificationController::class, 'userStore'])->name('notification.userStore');

Route::group(['prefix' => 'admin'], function () {
	Voyager::routes();
});

//Route::get('sequence', [\App\Http\Controllers\AuthController::class, 'generateSequence']);
