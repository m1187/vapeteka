<?php

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\ForgotPasswordController;
use App\Http\Controllers\VerifyPhoneController;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\IntegrationController;
use App\Http\Controllers\CatalogController;
use App\Http\Controllers\OrderController;
use App\Http\Controllers\HasCardController;
use App\Http\Controllers\ProductController;
use App\Http\Controllers\NotificationController;
use App\Http\Controllers\QrController;
use App\Http\Controllers\UserFixController;

Route::post('deleteDuplicateUsers', [UserFixController::class, 'deleteDuplicateUsers']);
Route::post('fixNumbers', [UserFixController::class, 'fixNumbers']);
Route::post('/register', [AuthController::class, 'register']);
Route::post('/has-card', [HasCardController::class, 'conf_phone']);

Route::post('/verify/phone', [VerifyPhoneController::class, 'postVerify']);

Route::get('/verification-send/{user}/{phone_number}', [\App\Http\Controllers\SendSmsController::class, 'sendSmsToVerify']);

Route::post('/login', [AuthController::class, 'login']);

Route::post('/forgot-password', [ForgotPasswordController::class, 'store']);
Route::post('/forgot-password/{phone_number}/update', [ForgotPasswordController::class, 'update']);

Route::post('/verification-resend', [VerifyPhoneController::class, 'resend']);

Route::prefix('integration')->group(function () {
	Route::post('/catalogs/store', [IntegrationController::class, 'storeCatalogs']);
	Route::post('/products/store', [IntegrationController::class, 'storeProducts']);
	Route::post('/users/store', [IntegrationController::class, 'storeUsers']);
});

Route::prefix('')
	->middleware(['auth:sanctum'])
	->group(function () {
        Route::post('/logout', [AuthController::class, 'logout']);

		Route::post('/order', [OrderController::class, 'store']);

		Route::get('/notifications', [NotificationController::class, 'index']);

		Route::get('/products', [ProductController::class, 'index'])->middleware(['checkUser']);
		Route::get('/products/{id}', [ProductController::class, 'show'])->middleware(['checkUser']);

        Route::get('/catalogs', [CatalogController::class, 'index'])->middleware('checkUser');
		Route::get('/catalogs/{id}', [CatalogController::class, 'show'])->middleware('checkUser');
        Route::get('/products/{id}', [CatalogController::class, 'products'])->middleware('checkUser');

		Route::post('/update-password', [AuthController::class, 'updatePassword']);

		Route::get('/profile', function () {
			$user = \App\Models\User::where('id', auth()->id())
				->select('id', 'name', 'phone_number', 'discount', 'cumulative')
				->first();

			return response(['user' => $user], 200);
		});

        Route::get('qr-discount', function () {
			$discount = \App\Models\User::where('id', auth()->id())
				->select('id', 'discount')
				->first();

			return response(['discount' => $discount], 200);
		});

		Route::get('qr-code', [QrController::class, 'show']);
		Route::post('get-token', function (Request $request) {
			$token = auth()->user()->mobile_token;
			if ($token == $request->mobile_token) {
				return response(['message' => 'Token is old'], 200);
			} else {
				$user = User::where('id', auth('sanctum')->id())->first();
				$user->mobile_token = $request->mobile_token;

				$user->save();

				return response(['message' => 'Token saved successfully'], 200);
			}
		});

        Route::post('delete-account', [AuthController::class, 'deleteAccount']);
	});
