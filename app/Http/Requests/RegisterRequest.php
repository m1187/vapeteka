<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Str;

class RegisterRequest extends FormRequest
{
	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize()
	{
		return true;
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		return [
			'first_name' => 'required',
			'last_name' => 'required',
			'middle_name' => '',
			'password' => ['required', 'min:4'],
			'conf_password' => ['required', 'min:4'],
			'phone_number' => ['required', 'regex:/^\d{11}$/'],
			'name' => [],
			'qr_code' => [],
			'qr_name' => [],
			'birthday' => ['required'],
		];
	}
}
