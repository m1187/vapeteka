<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\SendSms\SendSms;
use App\Traits\Message;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class ForgotPasswordController extends Controller
{
	use Message;

	public function store(Request $request)
	{
		$request->validate([
			'phone_number' => ['required', 'string', 'regex:/^\d{11}$/'],
		]);

		$user = User::where('phone_number', $request->phone_number)->first();

		if (!$user) {
			return response(['message' => 'Phone number not found'], 418);
		}

		$token = Str::random(32);

		DB::table('password_resets')->updateOrInsert(['email' => $user->phone_number], ['token' => $token]);

		$code = $this->generateFourDigitCode();
		// $code = '1111';
		$user->update([
			'code' => $code,
		]);

		$user->save();

		$validation_message = 'wait';
		if (!$user->phone_verification_send or Carbon::create($user->phone_verification_send)->addSeconds(60) >= Carbon::now()) {
			$message = $this->getMessage($user, $code);

			SendSms::sendSms($user->phone_number, $message);

			$user->update(['phone_verification_send' => Carbon::now()]);
		}

		return response(['message' => 'reset password sms sent', 'resetToken' => $token, 'phone_number' => $user->phone_number], 200);
	}

	public function update(Request $request)
	{
		$request->validate([
			'new_password' => ['required', 'string'],
			'conf_password' => ['required', 'string'],
			'phone_number' => ['required'],
		]);

		$user = User::where('phone_number', $request->phone_number)->first();

		$passwordReset = DB::table('password_resets')
			->where('email', $user->phone_number)
			->first();

		if ($request->new_password != $request->conf_password) {
			return response(['Incorrect password'], 418);
		}

		if ($passwordReset->token != $request->resetToken) {
			return response('Incorrect token', 418);
		}

		$user->password = Hash::make($request->new_password);

		$user->save();

		DB::table('password_resets')
			->where('email', $user->phone_number)
			->delete();

		return response(['message' => 'password updated'], 200);
	}
}
