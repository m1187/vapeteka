<?php

namespace App\Http\Controllers;

use App\Http\Requests\OrderStoreRequest;
use App\Models\Order;
use App\Models\OrderDetail;
use App\Models\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Notifications\Notifiable;
use App\Notifications\TelegramNotification;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Validator;
use Ramsey\Uuid\Uuid;

class OrderController extends Controller
{
	public function store(OrderStoreRequest $request)
	{
		$request->validated();

		$requestProducts = $request->products;

		$productPrice = 0;
		foreach ($requestProducts as $requestProduct) {
			$product = Product::where('id', $requestProduct['product_id'])->first();
			$productPrice = $productPrice + $product->price * $requestProduct['quantity'];
		}
		$bonusPrice = $productPrice - ($productPrice * (int) auth('sanctum')->user()->discount) / 100;

		$order = Order::create([
			'user_id' => auth('sanctum')->user()->id,
			'name' => auth('sanctum')->user()->name,
			'phone_number' => auth('sanctum')->user()->phone_number,
			'total_price' => $productPrice,
			'bonus_price' => $bonusPrice,
		]);

		foreach ($requestProducts as $requestProduct) {
			$product = Product::where('id', $requestProduct['product_id'])->first();
			OrderDetail::create([
				'order_id' => $order->id,
				'product_id' => $product->id,
				'price' => $product->price,
				'name' => $product->name,
				'quantity' => $requestProduct['quantity'],
			]);
		}

		$notice = new \App\Models\TelegramNotification([
			'id' => Uuid::uuid4()->toString(),
			'notice' => 'Новый заказ',
			'noticedes' => 'Номер Заказа: #' . $order->id,
			'total_price' => 'Общая сумма: ' . $order->total_price . 'тг',
			'noteicelink' => 'http://159.223.0.39/admin/orders/' . $order->id,
			'telegramid' => Config::get('services.telegram_id'),
		]);
		$notice->save();

		$notice->notify(new TelegramNotification());

		return response(['message' => 'Order created successfully'], 200);
	}
}
