<?php

namespace App\Http\Controllers\Voyager;

use Illuminate\Http\Request;
use TCG\Voyager\Http\Controllers\VoyagerBaseController;

class ShowController extends VoyagerBaseController
{
	public function index(Request $request)
	{
		return $this->show($request, 1);
	}
}
