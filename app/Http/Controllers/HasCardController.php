<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\SendSms\SendSms;
use App\Traits\Message;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class HasCardController extends Controller
{
	use Message;

	public function conf_phone(Request $request)
	{
		$request->validate([
			'qr_code' => 'required',
			'phone_number' => 'required',
			'password' => 'required',
			'conf_password' => 'required',
			'birthday' => 'required',
		]);

		$user = User::where('qr_code', $request->qr_code)
			->where('phone_number', $request->phone_number)
			->first();

		if (!$user) {
			return response(['message' => 'Not found user'], 204);
		}

		if ($user->phone_verified_at) {
			return response(['message' => 'User registered'], 418);
		}

		if ($request->password != $request->conf_password) {
			return response(['message' => 'password is not correct'], 204);
		}

        if (Carbon::create($request->birthday)->addYears(21) > Carbon::now()) {
			return response(['message' => 'Your birth day less than 21'], 205);
		}

		$user->password = \Hash::make($request->password);

		$user->save();

				$code = $this->generateFourDigitCode();
		// $code = '1111';
		$user->update(['code' => $code]);

		if (!$user->phone_verification_send or Carbon::create($user->phone_verification_send)->addSeconds(60) >= Carbon::now()) {
			$message = $this->getMessage($user, $code);

			SendSms::sendSms($user->phone_number, $message);

			$user->update(['phone_verification_send' => Carbon::now()]);

			$user->save();
		}

		return response(['message' => 'User has card', 'phone_number' => $user->phone_number, 'withCard' => 'with card'], 200);
	}
}
