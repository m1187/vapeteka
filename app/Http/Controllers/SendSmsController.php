<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\SendSms\SendSms;
use App\Traits\Message;
use Carbon\Carbon;

class SendSmsController extends Controller
{
	use Message;

	public function sendSmsToVerify(User $user, $phone_number)
	{
		$code = $this->generateFourDigitCode();

		$user->update([
			'code' => $code,
		]);

		$user->save();

		$validation_message = 'wait';
		if (!$user->phone_verification_send or Carbon::create($user->phone_verification_send)->addSeconds(60) >= Carbon::now()) {
			//add second to don't send resend sms
			$message = $this->getMessage($user, $code);

			SendSms::sendSms($user->phone_number, $message);

			$user->update(['phone_verification_send' => Carbon::now()]);

			$validation_message = null;
		}

		return response(['user' => $user], 200);
	}
}
