<?php

namespace App\Http\Controllers;

use App\Models\Token;
use App\Models\User;
use App\SendSms\SendSms;
use App\Traits\Message;
use Carbon\Carbon;
use Illuminate\Contracts\Auth\StatefulGuard;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Validator;
use Spatie\ArrayToXml\ArrayToXml;

class VerifyPhoneController extends Controller
{
	use Message;

	public function postVerify(Request $request)
	{
		Validator::make($request->input(), [
			'code' => ['required', 'regex:/^\d{4}$/'],
		])->validate();

		$user = User::where('phone_number', $request->phone_number)->first();

		if ($user->code !== (int) $request->code) {
			return response()->json('You entered wrong code', 418);
		}

		$isRegistering = !$user->phone_verified_at;
        $token = Token::find(1);

        if($request->registered == 'registered'){
            $user->update([
				'code' => null,
				'phone_verified_at' => now(),
                'check_account' => false,
			]);

			$user->save();

			return response(
				[
					'token' => $user->createToken('API Token')->plainTextToken,
				],
				200
			);
        }

		if ($isRegistering and $request->registered == 'register') {
            $bday = date('Y-m-d', strtotime($user->birthday));
			$body = "<x:Envelope
                xmlns:x='http://schemas.xmlsoap.org/soap/envelope/'
                xmlns:vap1='http://vap'>
                <x:Header/>
                <x:Body>
                    <vap1:store>
                        <vap1:name>$user->qr_name</vap1:name>
                        <vap1:card>$user->qr_code</vap1:card>
                        <vap1:owner>$user->name</vap1:owner>
                        <vap1:cumulative>$user->cumulative</vap1:cumulative>
                        <vap1:preset>$user->preset</vap1:preset>
                        <vap1:token>$token->token</vap1:token>
                        <vap1:Phone>$user->phone_number</vap1:Phone>
                        <vap1:dob>$bday</vap1:dob>
                    </vap1:store>
                </x:Body>
            </x:Envelope>";

			$response = Http::withBasicAuth(env('1C_LOGIN'), env('1C_PASSWORD'))
				->withBody($body, 'text/xml')
				->post('http://217.11.79.110/vape2/ws/ws1Client.1cws')
				->body();

			$clean_xml = str_ireplace(['SOAP-ENV:', 'SOAP:', 'M:'], '', $response);
			$xmlObject = simplexml_load_string($clean_xml);

			$json = json_encode($xmlObject);
			$phpDataArray = json_decode($json, true);

			if ($phpDataArray['Body']['storeResponse']['return']['code'] == 'error') {
				return response(['message' => 'problems from 1c'], 500);
			}

			$user->integration_id = $phpDataArray['Body']['storeResponse']['return']['code'];

			$user->update([
				'code' => null,
				'phone_verified_at' => now(),
			]);

			$user->save();

			return response(
				[
					'token' => $user->createToken('API Token')->plainTextToken,
				],
				200
			);
		} elseif ($request->withCard == 'with card') {
			$user->update([
				'code' => null,
				'phone_verified_at' => now(),
			]);

			$user->save();

			return response(
				[
					'token' => $user->createToken('API Token')->plainTextToken,
				],
				200
			);
		} else {
			$passwordReset = DB::table('password_resets')
				->where('email', $user->phone_number)
				->first();
			if ($request->resetToken != $passwordReset->token) {
				return response('Incorrect Token', 418);
			}

			$user->update([
				'code' => null,
			]);

			$user->save();

			return response(['reseted password code correct', 'phone_number' => $user->phone_number, 'resetToken' => $request->resetToken], 200);
		}
	}

	public function resend(Request $request)
	{
		$user = User::where('phone_number', $request->input('phone_number'))->first();

		$code = $this->generateFourDigitCode();

		$user->update([
			'code' => $code,
		]);

		$user->save();

		if (!$user->phone_verification_send or Carbon::create($user->phone_verification_send)->addSeconds(60) >= Carbon::now()) {
			return response(['message' => 'You cant send message now wait 1 minute'], 418);
		}

		$message = $this->getMessage($user, $code);

		SendSms::sendSms($user->phone_number, $message);

		$user->update(['phone_verification_send' => Carbon::now()]);

		return response(['message' => 'resented sms'], 200);
	}
}
