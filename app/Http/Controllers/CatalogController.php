<?php

namespace App\Http\Controllers;

use App\Models\Catalog;
use App\Models\Category;
use App\Models\Product;
use DB;
use Illuminate\Http\Request;

class CatalogController extends Controller
{
    public function index()
    {
        $catalogs = Category::get();

        return response(['catalogs' => $catalogs], 200);
    }

	public function show(Request $request)
	{
		$catalog = Category::where('id', $request->id)->first();

		return response(['catalog' => $catalog], 200);
	}

    public function products(Request $request)
    {
        $products = Product::where('category_id', $request->id)
            ->where('check', true)
            ->get();

        return response(['products' => $products], 200);
    }
}
