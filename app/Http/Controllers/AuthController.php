<?php

namespace App\Http\Controllers;

use App\Traits\Message;
use Illuminate\Http\Request;
use App\Traits\ApiResponser;
use App\Http\Requests\RegisterRequest;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use App\Models\User;
use App\SendSms\SendSms;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;

class AuthController extends Controller
{
	use ApiResponser;
	use Message;

    public function generateEAN($number)
    {
        $codePartials = str_split($number);
        $checkdigit = null;
        $evenNumbers = 0;
        $oddNumbers = 0;

        foreach ($codePartials as $key => $value) {
            if (($key + 1) % 2 == 0) { // Keys start from 0, We want the start to be 1
                $evenNumbers += $value;
            } else {
                $oddNumbers += $value;
            }
        }

        $evenNumbers = $evenNumbers * 3;
        $total = $evenNumbers + $oddNumbers;

        if ($total % 10 == 0) {
            $checkdigit = 0;
        } else {
            $next_multiple = $total + (10 - $total % 10);

            $checkdigit = $next_multiple - $total;
        }

        return $number . $checkdigit;
    }

	public function generateSequence(): int
	{
		/* Check Last value */
		$last_value = DB::table('users')
			->latest('qr_code')
			->first();

        $last_value_qr = substr($last_value->qr_code, 0, -1);

		/* Check if value not legacy */
		if ($last_value_qr >= 777000007000) {
			/* Generate value */
			return ++$last_value_qr;
		} else {
			/* Generate starting new value */
			return 777000007000;
		}
	}

	public function generateSequenceName($sequence): int|string
	{
		if (is_numeric($sequence)) {
			/* Trim first 777 characters */
			$sequence = ltrim($sequence, '777');

			/* Trim first zeros */
			$sequence = ltrim($sequence, '0');

			if ($sequence == '') {
				return 0;
			}

			return $sequence;
		} else {
			return 'value is not numeric';
		}
	}

	public function register(RegisterRequest $request)
	{
		$validatedData = $request->validated();
        $user = User::where('phone_number', $validatedData['phone_number'])
            ->where('check_account', true)
            ->first();

        if($user){
            if ($validatedData['password'] != $validatedData['conf_password']) {
                return response(['message' => 'password is not correct'], 422);
            }
            if (Carbon::createFromFormat('d/m/Y', $validatedData['birthday'])->addYears(21) > Carbon::now()) {
                return response(['message' => 'Your birth day less than 21'], 205);
            }

            $validatedData['password'] = Hash::make($validatedData['password']);

            $user->update(['password' => $validatedData['password']]);

            $user->save();

            return $this->sendSmsUpdateUserCode($user, 'registered');
        }

        $user = User::where('phone_number', $validatedData['phone_number'])
            ->where('check_account', false)
            ->first();

        if($user){
            return response(['message' => 'Your number is registered'], 400);
        }

		$validatedData['name'] = $validatedData['last_name'] . ' ' . $validatedData['first_name'] . ' ' . $validatedData['middle_name'];

		if ($validatedData['password'] != $validatedData['conf_password']) {
			return response(['message' => 'password is not correct'], 422);
		}
		if (Carbon::createFromFormat('d/m/Y', $validatedData['birthday'])->addYears(21) > Carbon::now()) {
			return response(['message' => 'Your birth day less than 21'], 205);
		}
		$qr = $this->generateSequence();

        $qr_ean = $this->generateEAN($qr);

		$validatedData['password'] = Hash::make($validatedData['password']);
		$validatedData['qr_code'] = $qr_ean;
		$validatedData['qr_name'] = $this->generateSequenceName($qr);

		unset($validatedData['first_name']);
		unset($validatedData['last_name']);
		unset($validatedData['middle_name']);
		unset($validatedData['conf_password']);

		$user = User::create($validatedData);

		return $this->sendSmsUpdateUserCode($user, 'register');
	}

	public function login(Request $request)
	{
		$credentials = $request->validate([
			'phone_number' => ['required', 'regex:/^\d{11}$/'],
			'password' => 'required',
		]);

		$user = User::where('phone_number', $request->phone_number)->first();

		if (!$user){
			return response(['message' => 'Your number or password is not correct'], 418);
		}

        if($user->check_account){
            return response(['message' => 'Not found User'], 400);
        }

		if (!$user) {
			return response(['error' => 'Not found User'], 400);
		}

		if ($user->phone_verified_at == null) {
			return response(['error' => 'not success number', 'phone_number' => $user->phone_number], 403);
		}

		if (!auth()->attempt($credentials)) {
			return response(['error' => 'The provided credentials are incorrect.'], 418);
		}

		return response(
			[
				'token' => auth('sanctum')
					->user()
					->createToken('API-Token')->plainTextToken,
			],
			200
		);
	}

	public function logout()
	{

        auth('sanctum')->user()->tokens()->delete();


		return response('Tokens Revoked', 200);
	}

	public function updatePassword(Request $request)
	{
		$request->validate([
			'old_password' => 'required',
			'new_password' => 'required',
			'conf_password' => 'required',
		]);

		$user = User::where('id', Auth::id())->first();

		if (Hash::check($request->old_password, $user->password)) {
			if ($request->new_password != $request->conf_password) {
				return response(['Incorrect password'], 418);
			}

			$user->password = Hash::make($request->new_password);

			$user->save();

			return response(['message' => 'password updated successfully'], 200);
		}

		return response(['message' => 'old password incorrect'], 204);
	}

    public function deleteAccount()
    {
        $user = User::where('id', auth('sanctum')->id())->first();

		$user->check_account = true;
        $user->phone_verified_at = null;
		$user->save();

		return response(['message' => 'Account deleted successfully'], 200);
    }
}
