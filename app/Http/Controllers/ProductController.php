<?php

namespace App\Http\Controllers;

use App\Models\Product;
use Illuminate\Http\Request;

class ProductController extends Controller
{
	public function index()
	{
		return response(['products' => Product::paginate(16)], 200);
	}

	public function show(Request $request)
	{
		$request->validate(['id' => 'required']);

		$product = Product::where('id', $request->id)->first();

		return response(['product' => $product], 200);
	}
}
