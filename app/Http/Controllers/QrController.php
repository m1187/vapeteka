<?php

namespace App\Http\Controllers;

use App\Models\Token;
use App\Models\TopUser;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;

class QrController extends Controller
{
    public function show()
    {
        $token = Token::where('id', 1)
            ->select('token')
            ->first();

        $checkUser = TopUser::where('user_id', auth()->id())->exists();
        // $token = "123123123123";
        $id = auth('sanctum')->user()->integration_id;
		$body = "<x:Envelope
                    xmlns:x='http://schemas.xmlsoap.org/soap/envelope/'
                    xmlns:vap1='http://vap'>
                    <x:Header/>
                    <x:Body>
                        <vap1:Get>
                        <vap1:token>$token->token</vap1:token>
                        <vap1:code>$id</vap1:code>
                        </vap1:Get>
                    </x:Body>
                </x:Envelope>";

		$response = Http::withBasicAuth(env('1C_LOGIN'), env('1C_PASSWORD'))
			->withBody($body, 'text/xml')
			->post('http://217.11.79.110/vape2/ws/ws1Client.1cws')
			->body();

		$clean_xml = str_ireplace(['SOAP-ENV:', 'SOAP:', 'M:'], '', $response);
		$xmlObject = simplexml_load_string($clean_xml);

		$json = str_replace(':{}', ':null', json_encode($xmlObject));

		$phpDataArray = json_decode($json, true);
		$user = $phpDataArray['Body']['GetResponse']['return']['ElemCard'];

        if(!$checkUser){
            auth('sanctum')->user()->update(['discount' => $user['percent'] ]);
        }

        auth('sanctum')->user()->update(['qr_scaner' => $user['qr_code'] ]);

        return response(['qr_code' => auth('sanctum')->user()->qr_scaner], 200);

    }
}
