<?php

namespace App\Http\Controllers;

use App\Models\Catalog;
use App\Models\Product;
use App\Models\Token;
use App\Models\User;
use Artisaninweb\SoapWrapper\SoapWrapper;
use GuzzleHttp\Client;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;

class IntegrationController extends Controller
{
	public function storeProducts(Request $request)
	{
		$token = Token::where('id', 1)
			->select('token')
			->first();
		$body = "<x:Envelope
                    xmlns:x='http://schemas.xmlsoap.org/soap/envelope/'
                    xmlns:vap1='http://vap'>
                    <x:Header/>
                    <x:Body>
                        <vap1:GetProducts>
                            <vap1:token>$token->token</vap1:token>
                        </vap1:GetProducts>
                    </x:Body>
                </x:Envelope>";

		$response = Http::withBasicAuth(env('1C_LOGIN'), env('1C_PASSWORD'))
			->withBody($body, 'text/xml')
			->post('http://217.11.79.110/vape2/ws/ws1Client.1cws')
			->body();

		$clean_xml = str_ireplace(['SOAP-ENV:', 'SOAP:', 'M:'], '', $response);
		$xmlObject = simplexml_load_string($clean_xml);

		$json = str_replace(':{}', ':null', json_encode($xmlObject));

		$phpDataArray = json_decode($json, true);

		$products = $phpDataArray['Body']['GetProductsResponse']['return']['good'];

		foreach ($products as $product) {
			Product::updateOrCreate(
				[
					'integration_id' => (int) $product['code'],
				],
				[
					'integration_id' => (int) $product['code'],
					'name' => $product['name'],
					'price' => (float) $product['price'],
					'catalog_id' => (int) $product['category_id'],
					'amount' => (int) $product['amount'],
					'brand' => $product['brand'],
					'flavor' => $product['flavor'],
					'nomenclature' => $product['nomenclature'],
					'class' => $product['class'],
					'power' => $product['power'],
					'volume' => $product['volume'],
					'made_in' => $product['made_in'],
					'item_no_discount' => $product['item_no_discount'],
					'wh_code' => $product['wh_code'],
					'wh_name' => $product['wh_name'],
				]
			);
		}

		return response(['message' => 'products created successfully'], 200);
	}

	public function storeCatalogs(Request $request)
	{
		$token = Token::where('id', 1)
			->select('token')
			->first();
		$body = "<x:Envelope
                    xmlns:x='http://schemas.xmlsoap.org/soap/envelope/'
                    xmlns:vap1='http://vap'>
                    <x:Header/>
                    <x:Body>
                        <vap1:GetProductsCategories>
                            <vap1:token>$token->token</vap1:token>
                        </vap1:GetProductsCategories>
                    </x:Body>
                </x:Envelope>";

		$response = Http::withBasicAuth(env('1C_LOGIN'), env('1C_PASSWORD'))
			->withBody($body, 'text/xml')
			->post('http://217.11.79.110/vape2/ws/ws1Client.1cws')
			->body();

		$clean_xml = str_ireplace(['SOAP-ENV:', 'SOAP:', 'M:'], '', $response);
		$xmlObject = simplexml_load_string($clean_xml);

		$json = str_replace(':{}', ':null', json_encode($xmlObject));

		$phpDataArray = json_decode($json, true);

		$catalogs = $phpDataArray['Body']['GetProductsCategoriesResponse']['return']['Category'];
		foreach ($catalogs as $catalog) {
			Catalog::updateOrCreate(
				[
					'integration_id' => $catalog['code'],
				],
				[
					'integration_id' => $catalog['code'],
					'catalog' => $catalog['name'],
					'catalog_id' => $catalog['category_id'],
				]
			);
		}

		return response(['message' => 'catalogs created successfully'], 200);
	}

	public function storeUsers(Request $request)
	{
		$token = Token::where('id', 1)
			->select('token')
			->first();
		$body = "<x:Envelope
                    xmlns:x='http://schemas.xmlsoap.org/soap/envelope/'
                    xmlns:vap1='http://vap'>
                    <x:Header/>
                    <x:Body>
                        <vap1:Get>
                            <vap1:token>$token->token</vap1:token>
                            <vap1:code></vap1:code>
                        </vap1:Get>
                    </x:Body>
                </x:Envelope>";

		$response = Http::withBasicAuth(env('1C_LOGIN'), env('1C_PASSWORD'))
			->withBody($body, 'text/xml')
			->post('http://217.11.79.110/vape2/ws/ws1Client.1cws')
			->body();

		$clean_xml = str_ireplace(['SOAP-ENV:', 'SOAP:', 'M:'], '', $response);
		$xmlObject = simplexml_load_string($clean_xml);

		$json = str_replace(':{}', ':null', json_encode($xmlObject));
		$phpDataArray = json_decode($json, true);

		$dates = $phpDataArray['Body']['GetResponse']['return']['ElemCard'];

		foreach ($dates as $date) {
			$date['cumulative'] = $date['cumulative'] == 'true' ? 1 : 0;
			//			dd($item['preset']);
			//			dd($item['name']);
			User::updateOrCreate(
				[
					'integration_id' => $date['code'],
				],
				[
					'name' => $date['owner'],
					'phone_number' => $date['phone'],
					'cumulative' => $date['cumulative'],
					'discount' => $date['percent'],
                    'birthday' => $date['dob'],
                    'qr_scaner' => $date['qr_code'],
                    'qr_code' => $date['card'],
                    'qr_name' => $date['name'],
				]
			);
		}

		return response(['message' => 'Clients created successfully'], 200);
	}
}
