<?php

namespace App\Http\Controllers;
use App\Models\NotificationSend;
use App\Models\OneUserNotification;
use App\Models\User;
use App\Traits\Push;
use Illuminate\Http\Request;

class NotificationController extends Controller
{
	use Push;

	public function store(Request $request)
	{
		$notification = NotificationSend::where('id', $request->id)->first();

		$this->sendPush($notification->title, $notification->content, '/topics/android');

		return redirect()->back();
	}

	public function userStore(Request $request)
	{
		$notification = OneUserNotification::where('id', $request->id)->first();
		$user = User::where('id', $notification->user_id)->first();

		$this->sendPush($notification->header, $notification->content, $user->mobile_token);

		return redirect()->back();
	}

	public function index()
	{
		return response(['notifications' => NotificationSend::orderBy('created_at', 'DESC')->get()], 200);
	}
}
