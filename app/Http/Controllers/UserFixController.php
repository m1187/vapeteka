<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;

class UserFixController extends Controller
{
    public function deleteDuplicateUsers()
    {
        $users = User::all();

        foreach($users as $user){
            $userDis = User::where('phone_number', $user->phone_number)
                ->where('id', '!=', $user->id)->get();

            foreach($userDis as $userDi){
                $deleteUser = $user;
                if($deleteUser->discount > $userDi->discount){
                    $userDi->delete();
                }elseif($deleteUser->integration_id < $userDi->integration_id){
                    $userDi->delete();
                }
                else{
                    $user = $deleteUser;
                    $deleteUser->delete();
                }
            }
        }

        return response(['message' => 'Success'], 200);
    }

    public function fixNumbers()
    {
        $users = User::all();

        foreach ($users as $user){
            $phone = str_replace(' ', '', $user->phone_number);
            $phone = str_replace('-', '', $user->phone_number);

            $len = strlen($phone);

            if($len == 11 and $phone[0] == 7){
                continue;
            }
            if($len == 10 and $phone[0] == 7){
                $phone = "7" . $phone;
                $user->phone_number = $phone;
                $user->save();
            }
            if($len == 11 and $phone[0] == "+"){
                $phone = ltrim($phone, "+");
                $phone = "7" . $phone;
                $user->phone_number = $phone;
                $user->save();
            }
            if($len == 12 and $phone[0] == "+"){
                $phone = ltrim($phone, "+");
                $user->phone_number = $phone;
                $user->save();
            }
            if($len == 11 and $phone[0] == 8){
                $phone = substr($phone, 1);
                $phone = "7" . $phone;
                $user->phone_number = $phone;
                $user->save();
            }
        }

        return response(['message' => 'Success'], 200);
    }
}
