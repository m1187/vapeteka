<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Catalog extends Model
{
	use HasFactory;

	protected $fillable = ['catalog', 'catalog_id', 'integration_id'];

	public function products()
	{
		return $this->hasMany(Product::class);
	}

	public function parent()
	{
		return $this->belongsTo(Catalog::class, 'catalog_id');
	}

	public function childrenRecursive()
	{
		return $this->childs()->with('childrenRecursive:id,catalog,catalog_id,integration_id');
	}

	public function childs()
	{
		return $this->hasMany(Catalog::class, 'catalog_id', 'integration_id');
	}

	public function subCatalogItems()
	{
		return $this->hasManyThrough('App\Models\Catalog', 'App\Models\Product', 'catalog_id', 'catalog_id', 'id', 'id');
	}
}
