<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class OrderDetail extends Model
{
	use HasFactory;

	protected $table = 'product_order';

	protected $guarded = [];

	public function products()
	{
		return $this->belongsToMany(Product::class, 'product_order', 'product_id');
	}
}
