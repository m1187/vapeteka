<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Notifications\Notification;

class TelegramNotification extends Model
{
	use HasFactory, Notifiable;

	protected $primaryKey = 'id';
	protected $keyType = 'string';

	public $incrementing = false;

	protected $guarded = [];
}
