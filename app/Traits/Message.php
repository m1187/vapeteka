<?php
namespace App\Traits;

use App\SendSms\SendSms;
use Carbon\Carbon;

trait Message
{
	public function sendSmsUpdateUserCode($user, $messageFromRegister)
	{
		$code = $this->generateFourDigitCode();
		// $code = '1111';

		$user->update(['code' => $code]);

		$user->save();

		$validation_message = 'wait';
		if (!$user->phone_verification_send or Carbon::create($user->phone_verification_send)->addSeconds(60) >= Carbon::now()) {
			//add second to don't send resend sms
			$message = $this->getMessage($user, $code);

			SendSms::sendSms($user->phone_number, $message);

			$user->update(['phone_verification_send' => Carbon::now()]);
		}

		return response(['message' => 'registered', 'phone_number' => $user->phone_number, 'registered' => $messageFromRegister], 200);
	}

	public function generateFourDigitCode()
	{
		$code = str_pad(mt_rand(1000, 9999), 4, '0', STR_PAD_LEFT);

		return $code;
	}

	public function getMessage($user, $code)
	{
		if (!$user->phone_verified_at) {
			$message = "$code - код для регистрации в приложении Vapeteka ";
		} else {
			$message = "$code - код для смены пароля в приложении Vapeteka ";
		}

		return $message;
	}
}
