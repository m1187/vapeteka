<?php

namespace App\SendSms;

use App\Models\User;
use Carbon\Carbon;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Str;

class SendSms
{
	public static function sendSms($phone_numbers, $message)
	{
		$link = 'https://smsc.kz/sys/send.php';

		if (is_array($phone_numbers)) {
			$phone_numbers = implode(';', $phone_numbers);
		}

		Http::get($link, [
			'login' => env('SMS_LOGIN'),
			'psw' => env('SMS_PASSWORD'), //delete 4
			'phones' => $phone_numbers,
			'mes' => $message,
		]);
	}
}
